function [K_2] = changement_base(A,C,K_1)

P = sylvester((A-K_1*C)',(A-K_1*C), -C'*C);    
K_2 = K_1 - inv(P)*C'; 
end