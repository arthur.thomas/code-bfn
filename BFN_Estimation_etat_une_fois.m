clear all
close all

%% Paramètres du système %%

A = [0  1; -1 0];
C = [1  0];
p = [-2,-1]; %valeurs propres voulues pour A-K_+*C
K_1 = place(A',C',p);
K_1 = K_1';
P = sylvester((A-K_1*C)',(A-K_1*C), -C'*C);
K_2 = changement_base(A,C,K_1);
% K_2 = place(-A',-C',p);
% K_2=K_2';
x0 = [1,0,5,-5]'; %condition initiale 
t0 = 0;
t1 = 30;
Nt = 1e4;
sigma = tau(t0);
N=[];
n = size(A,1);
delta = (t1-t0)/Nt;
Sigma = [];
%% Estimation état %%

syst1 = @(t,X) f(A,X);
[T,X] = ode45(syst1,linspace(t0,t1,Nt),x0(1:n));

syst = @(t,x) tau(t);
[T,Tau] = ode45(syst,linspace(t0,t1,Nt),0);

for k = 1:size(Tau,1)
   if Tau(k)>T(k)
        Tau(k)=T(k);
   end
    if Tau(k)<0
        Tau(k)=0;
    end
end
F =griddedInterpolant(T,X);
Xbis = interp1(T,X,Tau);

syst2 = @(t,Xhat) fhat(t,T,Tau,Xbis,A,C,K_1,K_2,Xhat);
[T2,Xhat] = ode45(syst2,linspace(t0,t1,Nt),x0(n+1:end));




 %% Tracé des courbes %%
% E = Xbis-Xhat;
%    for i = 1:length(E)
%        N(i)=norm(E(i,:));
%    end
V = zeros(size(Xbis,1),1);
for k =1:size(Xbis,1)
    V(k)= 1/2*(Xbis(k,:)-Xhat(k,:))*P*(Xbis(k,:)-Xhat(k,:))';
end
plot (Xhat(:,1), Xhat(:,2))
xlabel ('Xhat_1');
ylabel ('Xhat_2');
hold on
%plot(Xbis(:,1),Xbis(:,2))

figure
%plot(T,N)
plot(T,V)
xlabel('t')
ylabel("V(x)")
hold on
%plot(T,Tau)

figure
plot(T,X(:,1))
hold on
plot(Tau,Xhat(:,1))
hold on
plot(T,X(:,2))
hold on 
plot(Tau,Xhat(:,2))
xlabel('t')
legend('X_1','Xhat_1','X_2','Xhat_2' )

figure
plot(T,T)
hold on
plot(T,Tau)
legend('T','Tau')
xlabel('t')













