function [xhatdot] = fhat_f_ode(xbis,xhat,A,K_1,C,sigma)
xhatdot = sigma*(A*xhat-K_1*C*(xhat-xbis')) ;
end