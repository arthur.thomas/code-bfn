function [Xhatdot] = fhat(t,T,Tau,Xbis,A,C,K_1,K_2,Xhat)
%disp(t)
sigma = tau(t);
i = cherche_indice(T,t);
if sigma>0
    if Tau(i)>=T(i)
        Xhatdot = fhat_f_ode(Xbis(i,:),Xhat,A,K_1,C,1);
    else
        Xhatdot = fhat_f_ode(Xbis(i,:),Xhat,A,K_1,C,sigma);
    end
else
    Xhatdot = fhat_f_ode(Xbis(i,:),Xhat,A,K_2,C,sigma);
end
