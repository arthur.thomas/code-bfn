function [i] = cherche_indice(T,t)
m=abs(T(1)-t);
i=1;
for k = 1:size(T,1)
    if abs(T(k)-t)<m
        m=abs(T(k)-t);
        i=k;
    end
    
end